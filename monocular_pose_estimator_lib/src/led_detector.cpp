// This file is part of RPG-MPE - the RPG Monocular Pose Estimator
//
// RPG-MPE is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RPG-MPE is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RPG-MPE.  If not, see <http://www.gnu.org/licenses/>.

/*
 * led_detector.cpp
 *
 * Created on: July 29, 2013
 * Author: Karl Schwabe
 */

/**
 * \file led_detector.cpp
 * \brief File containing the function definitions required for detecting LEDs and visualising their detections and the pose of the tracked object.
 *
 */

#include "monocular_pose_estimator_lib/led_detector.h"

namespace monocular_pose_estimator
{

// LED detector
void LEDDetector::findLeds(const cv::Mat &image, cv::Rect ROI, const int &threshold_value, const double &gaussian_sigma,
                           const double &min_blob_area, const double &max_blob_area,
                           const double &max_width_height_distortion, const double &max_circular_distortion,
                           List2DPoints &pixel_positions, List2DPoints &distorted_input_data,
                           const cv::Mat &camera_matrix_K, const std::vector<double> &camera_distortion_coeffs)
{
  // Threshold the image
  cv::Mat bw_image;
  
  //cv::threshold(image, bwImage, threshold_value, 255, cv::THRESH_BINARY);
  cv::threshold(image(ROI), bw_image, threshold_value, 255, cv::THRESH_TOZERO);
  
  // Gaussian blur the image
  cv::Mat gaussian_image;
  cv::Size ksize; // Gaussian kernel size. If equal to zero, then the kerenl size is computed from the sigma
  ksize.width = 0;
  ksize.height = 0;
  
  if (gaussian_sigma != 0)
  {
    
    GaussianBlur(bw_image.clone(), gaussian_image, ksize, gaussian_sigma, gaussian_sigma, cv::BORDER_DEFAULT);
  } 
  else // Dont use guassian blur
  {
    gaussian_image = bw_image;
  }

  
  //cv::imshow( "Gaussian", gaussian_image );
  
  // Find all contours
  std::vector<std::vector<cv::Point> > contours;
  cv::findContours(gaussian_image.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
  
  unsigned numPoints = 0; // Counter for the number of detected LEDs

  // Vector for containing the detected points that will be undistorted later
  std::vector<cv::Point2f> distorted_points;

  // Identify the blobs in the image
  for (unsigned i = 0; i < contours.size(); i++)
  {
    double area = cv::contourArea(contours[i]); // Blob area
    cv::Rect rect = cv::boundingRect(contours[i]); // Bounding box
    double radius = (rect.width + rect.height) / 4; // Average radius

    cv::Moments mu;

    mu = cv::moments(contours[i], false);
    cv::Point2f mc;
    mc = cv::Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00) + cv::Point2f(ROI.x, ROI.y);
    // Look for round shaped blobs of the correct size
    if (area >= min_blob_area && area <= max_blob_area
        && std::abs(1 - std::min((double)rect.width / (double)rect.height, (double)rect.height / (double)rect.width))
            <= max_width_height_distortion
        && std::abs(1 - (area / (CV_PI * std::pow(rect.width / 2, 2)))) <= max_circular_distortion
        && std::abs(1 - (area / (CV_PI * std::pow(rect.height / 2, 2)))) <= max_circular_distortion)
    {
      distorted_points.push_back(mc);
      numPoints++;
    }
  }

  // Convert to List2DPoints
  distorted_input_data.resize(distorted_points.size());

  for (unsigned i = 0; i < distorted_points.size(); ++i)
  {
    distorted_input_data[i](0) = distorted_points[i].x;
    distorted_input_data[i](1) = distorted_points[i].y;
  }
  

  if (numPoints > 0)
  {
    // Vector that will contain the undistorted points
    std::vector<cv::Point2f> undistorted_points;

    // Undistort the points
    cv::undistortPoints(distorted_points, undistorted_points, camera_matrix_K, camera_distortion_coeffs, cv::noArray(),
                        camera_matrix_K);
    
    // Resize the vector to hold all the possible LED points
    pixel_positions.resize(numPoints);

    // Populate the output vector of points
    for (unsigned j = 0; j < numPoints; ++j)
    {
      Eigen::Vector2d point;
      point(0) = undistorted_points[j].x;
      point(1) = undistorted_points[j].y;
      pixel_positions(j) = point;
    }
  }
}

cv::Rect LEDDetector::determineROI(List2DPoints pixel_positions, cv::Size image_size, const int border_size,
                                   const cv::Mat &camera_matrix_K, const std::vector<double> &camera_distortion_coeffs)
{
  double x_min = INFINITY;
  double x_max = 0;
  double y_min = INFINITY;
  double y_max = 0;

  for (unsigned i = 0; i < pixel_positions.size(); ++i)
  {
    if (pixel_positions(i)(0) < x_min)
    {
      x_min = pixel_positions(i)(0);
    }
    if (pixel_positions(i)(0) > x_max)
    {
      x_max = pixel_positions(i)(0);
    }
    if (pixel_positions(i)(1) < y_min)
    {
      y_min = pixel_positions(i)(1);
    }
    if (pixel_positions(i)(1) > y_max)
    {
      y_max = pixel_positions(i)(1);
    }
  }

  std::vector<cv::Point2f> undistorted_points;

  undistorted_points.push_back(cv::Point2f(x_min, y_min));
  undistorted_points.push_back(cv::Point2f(x_max, y_max));

  std::vector<cv::Point2f> distorted_points;

  // Distort the points
  distortPoints(undistorted_points, distorted_points, camera_matrix_K, camera_distortion_coeffs);

  double x_min_dist = distorted_points[0].x;
  double y_min_dist = distorted_points[0].y;
  double x_max_dist = distorted_points[1].x;
  double y_max_dist = distorted_points[1].y;

  double x0 = std::max(0.0, std::min((double)image_size.width, x_min_dist - border_size));
  double x1 = std::max(0.0, std::min((double)image_size.width, x_max_dist + border_size));
  double y0 = std::max(0.0, std::min((double)image_size.height, y_min_dist - border_size));
  double y1 = std::max(0.0, std::min((double)image_size.height, y_max_dist + border_size));

  cv::Rect region_of_interest;

  // if region of interest is too small, use entire image
  // (this happens, e.g., if prediction is outside of the image)
  if (x1 - x0 < 1 || y1 - y0 < 1)
  {
    region_of_interest = cv::Rect(0, 0, image_size.width, image_size.height);
  }
  else
  {
    region_of_interest.x = x0;
    region_of_interest.y = y0;
    region_of_interest.width = x1 - x0;
    region_of_interest.height = y1 - y0;
  }

  return region_of_interest;
}

void LEDDetector::distortPoints(const std::vector<cv::Point2f> & src, std::vector<cv::Point2f> & dst,
                                const cv::Mat & camera_matrix_K, const std::vector<double> & distortion_matrix)
{
  dst.clear();
  double fx_K = camera_matrix_K.at<double>(0, 0);
  double fy_K = camera_matrix_K.at<double>(1, 1);
  double cx_K = camera_matrix_K.at<double>(0, 2);
  double cy_K = camera_matrix_K.at<double>(1, 2);

  double k1 = distortion_matrix[0];
  double k2 = distortion_matrix[1];
  double p1 = distortion_matrix[2];
  double p2 = distortion_matrix[3];
  double k3 = distortion_matrix[4];

  for (unsigned int i = 0; i < src.size(); i++)
  {
    // Project the points into the world
    const cv::Point2d &p = src[i];
    double x = (p.x - cx_K) / fx_K;
    double y = (p.y - cy_K) / fy_K;
    double xCorrected, yCorrected;

    // Correct distortion
    {
      double r2 = x * x + y * y;

      // Radial distortion
      xCorrected = x * (1. + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2);
      yCorrected = y * (1. + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2);

      // Tangential distortion
      xCorrected = xCorrected + (2. * p1 * x * y + p2 * (r2 + 2. * x * x));
      yCorrected = yCorrected + (p1 * (r2 + 2. * y * y) + 2. * p2 * x * y);
    }

    // Project coordinates onto image plane
    {
      xCorrected = xCorrected * fx_K + cx_K;
      yCorrected = yCorrected * fy_K + cy_K;
    }
    dst.push_back(cv::Point2d(xCorrected, yCorrected));
  }
}

Eigen::MatrixXd LEDDetector::calculate_distance(List2DPoints& data_points1, List2DPoints& data_points2)
{
  /* This function calculates the distance from an list of points to another list of points.
  The function return a Mat array with the 2Norm distances.*/

  // Gets size of input data 1
  int data1 = data_points1.rows();

  // Gets size of input data 1
  int data2 = data_points2.rows();
  
  // Initialises variable
  Eigen::MatrixXd distance_matrix(data1, data2);

  // Calculates distances
  for (int i = 0; i < data1; i++)
  {
    for (int j = 0; j < data2; j++)
    {
      Eigen::Vector2d temp_diff;
      temp_diff = data_points1(i) - data_points2(j);
      distance_matrix(i, j) = temp_diff.norm();
    }
  }
  return distance_matrix;
}

Eigen::MatrixXd LEDDetector::calculate_distance_single_point(Eigen::Vector2d& data_point_poi, List2DPoints& data_points)
{
  /* This function calculates the distance from a single point to a list of points.
  The function return a Mat array with the 2Norm distances.*/

  // Gets size of input data 1
  int data1 = 1;

  // Gets size of input data 1
  int data2 = data_points.rows();
  
  // Initialises variable
  Eigen::MatrixXd distance_vector(data1, data2);

  // Calculates distances
  for (int i = 0; i < data1; i++)
  {
    for (int j = 0; j < data2; j++)
    {
      Eigen::Vector2d temp_diff;
      temp_diff = data_point_poi - data_points(j);
      distance_vector(i, j) = temp_diff.norm();
    }
  }
  return distance_vector;
}

void LEDDetector::outlier_detection_density(const int num_led_objects, List2DPoints& input_data, List2DPoints& distorted_input_data, double &min_point_density_threshold)
{
  // Gets size of input data
  int rows = input_data.rows();

  // Calculate distance matrix
  Eigen::MatrixXd distance_to_points = calculate_distance(input_data, input_data);

  // Initialize density vector
  Eigen::MatrixXd density_vector = Eigen::MatrixXd::Zero(rows, 1);

  // Caldulates density for every point
  for (int i = 0; i < rows; i++)
  {
    // Sets diagonal element to infinity
    distance_to_points(i,i) = INFINITY;

    // Gets submatrix for the data point
    Eigen::MatrixXd distance_to_single_point = distance_to_points.col(i);

    Eigen::MatrixXd distance_vector = Eigen::MatrixXd::Zero(num_led_objects-1, 1);
    for (int k = 0; k < num_led_objects-1; k++)
    {
      // Finds minimum in distance matrix
      Eigen::MatrixXd::Index minRow, minCol;
      double min = distance_to_single_point.minCoeff(&minRow,&minCol);
      distance_to_single_point(minRow,0) = INFINITY;

      // Saves the minimum distance
      distance_vector(k) = min;
    }

    // Calculates the density
    density_vector(i,0) = 1/distance_vector.mean();
  }

  for (int i = rows-1; i >= 0; i--)
  {
    if (density_vector(i,0) < min_point_density_threshold)
    {
      unsigned int numRows = input_data.rows()-1;

      if( i < numRows )
      {
        input_data.segment(i, numRows - i) = input_data.segment(i + 1, numRows - i);
        distorted_input_data.segment(i, numRows - i) = distorted_input_data.segment(i + 1, numRows - i);
      }
      input_data.conservativeResize(numRows);
      distorted_input_data.conservativeResize(numRows);
    }
  }
}

void LEDDetector::kmeans_cluster(const int num_led_objects, List2DPoints& input_data, List2DPoints& cluster_centers, VectorList2DPoints& sorted_led_pos, List2DPoints& distorted_input_data, VectorList2DPoints& sorted_distortion_centers, RowXs& cluster_correspondences, double& max_relative_cluster_distance, double& max_led_to_cluster_distance, double& min_point_density_threshold)
{
  /* The kmeans_clust() takes in a list of 2Dpoints (pixel values) and cluster them according to the kmeans++ algorithm
  and outputs the label for the individual points, and the centers of the cluster*/
  // input_data.conservativeResize(0);
  // Gets size of input data
  int rows = input_data.rows();

  // Approximates number of cluster
  int clusters = round(rows / (float)num_led_objects);
  if (clusters != 0)
  {
    outlier_detection_density(num_led_objects, input_data, distorted_input_data,  min_point_density_threshold);
  }
  // Calculates number of clusters (again)
  rows = input_data.rows();

  clusters = round(rows / (float)num_led_objects);

  if (clusters != 0)
  {

    int cols = input_data(0).size();
    // Gets number of cluster centers

    int clust_rows = cluster_centers.rows(), clust_cols = 2; //cluster_centers(0).size();

  
    // Creates CV mat, and puts input_data into it
    cv::Mat data_points(rows,cols,CV_32F);
    for (int i = 0; i < rows; i++)
    {
      for (int j = 0; j < cols; j++)
        data_points.at<float>(i, j) = input_data(i)(j);
    }

    
    // Creates object containin class labels
    cv::Mat labels = cv::Mat::zeros(rows, 1, CV_32S);
    // Creates object containing center
    //cv::Mat centers_cv;

    cv::Mat centers_cv(clust_rows, clust_cols, CV_32F);

    // Creates list containing old centers
    List2DPoints cluster_centers_old;
    cluster_centers_old = cluster_centers;


    bool converted_flag = false;
    while (converted_flag == false)
    {
      // Runs K-means algo with random centers
      cv::kmeans(data_points, clusters, labels,
        cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 10, 1.0),
        3, cv::KMEANS_PP_CENTERS, centers_cv);
      
      
      ///* Updates the centers, and saves old centers in variable
      // Updates cluster centers to new value
      cluster_centers.resize(clusters);
      for (int i = 0; i < clusters; i++)
      {
        for (int j = 0; j < clust_cols; j++)
          cluster_centers(i)(j) = centers_cv.at<float>(i, j);
      }

      ///* Puts data into a list of clusters 
      // Creates variable to hold number of leds per cluster
      Eigen::MatrixXd leds_clusters = Eigen::MatrixXd::Zero(clusters, 1);
      for (int i = 0; i < rows; i++)
      {
        // Counts LED's in each cluster
        int temp_label = labels.at<int>(i, 0);
        leds_clusters(temp_label) = leds_clusters(temp_label) + 1;  // Dont know why type has to be char
      }

      // Puts each cluster into its own matrix (now also with distorted data
      // Gives the variable the correct dimension
      sorted_led_pos.resize(clusters);
      sorted_distortion_centers.resize(clusters);
      for (int i = 0; i < clusters; i++)
      {
        sorted_led_pos(i).resize(leds_clusters(i));
        sorted_distortion_centers(i).resize(leds_clusters(i));
      }
      // Used to keep track of sorting the led positions (OUTPUT OF FUNCTION)
      Eigen::MatrixXd leds_index = Eigen::MatrixXd::Zero(clusters, 1);
      for (int i = 0; i < rows; i++)
      {
        int temp_label = labels.at<int>(i, 0);
        sorted_led_pos(temp_label)((int)leds_index(temp_label, 0)) = input_data(i);
        sorted_distortion_centers(temp_label)((int)leds_index(temp_label, 0)) = distorted_input_data(i);
        leds_index(temp_label) = leds_index(temp_label, 0) + 1;
      }

      ///* Continues to move points between cluster, until every cluster is saturated, or the distance is to high 
      bool flag_move_point = true;
      while (flag_move_point)
      {
        distributePointToDeficit(flag_move_point, num_led_objects, leds_clusters, sorted_led_pos, cluster_centers, sorted_distortion_centers, max_led_to_cluster_distance);
      }

      ///* Moves data using KNN 
      //flag_move_point = true;
      //while (flag_move_point)
      //{
      //  distributePointKNN(flag_move_point, num_led_objects, leds_clusters, sorted_led_pos, cluster_centers, sorted_distortion_centers, max_led_to_cluster_distance);
        //flag_move_point = false;
      //}

      if(leds_clusters.maxCoeff() < num_led_objects*2)
      {
        converted_flag = true;
      }
      else
      {
        clusters = clusters + 1;
      }
      
    }
    // Finds corresponcences between old and new cluster centers
    findCenterCorrespondence(cluster_correspondences, cluster_centers_old, cluster_centers, max_relative_cluster_distance);
  }
  else
  {
    cluster_centers.conservativeResize(0); 
    sorted_led_pos.conservativeResize(0);
    distorted_input_data.conservativeResize(0);
    sorted_distortion_centers.conservativeResize(0);
  }
}

void LEDDetector::distributePointKNN(bool& flag_move_point, const int& num_led_objects, Eigen::MatrixXd& leds_clusters, VectorList2DPoints& sorted_led_pos, List2DPoints& cluster_centers, VectorList2DPoints& sorted_distortion_centers, double& max_led_to_cluster_distance)
{
  /* Move a data point from a cluster with many points, to a cluster with fewer point using KNN, unless the distance is to high */
  // Finds number of clusters
  int clusters = sorted_led_pos.size();

  // Creates flag to indicate if any cluster has more than five leds
  int cluster_overload = 0;
  // Creates flag to indicate if any cluster has five leds
  int cluster_avgload = 0;
  // Checks the led amount in each cluster
  for (int i = 0; i < clusters; i++)
  {
    // Checks if a cluster has more than 5 leds
    if (leds_clusters(i) > num_led_objects)
    {
      cluster_overload = cluster_overload + 1;
    }
    // Checks if a cluster has less than 5 leds
    if (leds_clusters(i) == num_led_objects)
    {
      cluster_avgload = cluster_avgload + 1;
    }
  }


  // If a cluster has a surplus and another has a average LEDS, the cluster with surplus should give out leds
  if (cluster_overload > 0 && cluster_avgload > 0)
  {
    // Creates variable to keep track of when the global minima distance has been reached
    double min_distance_led_deficit = max_led_to_cluster_distance + 1;
    int min_distance_row;
    int min_distance_col;
    int min_distance_cluster;

    // Creates variable to hold the centers of the cluster with deficit leds
    List2DPoints led_avg_center;
    led_avg_center.resize(cluster_avgload);
    int temp_idx = 0;
    // Runs through all clusters and finds cluster with deficit leds
    for (int i = 0; i < clusters; i++)
    {
      // Finds cluster with deficit LEDS
      if (leds_clusters(i) < num_led_objects)
      {
        // Puts the deficit centers into a matrix
        led_avg_center(temp_idx) = cluster_centers(i);
        temp_idx += 1;
      }

    }
    for (int i = 0; i < clusters; i++)
    {
      // Finds cluster with surplus LEDS
      if (leds_clusters(i) > num_led_objects)
      {
        // Calculates distance between surplus leds and deficit cluster centers
        Eigen::MatrixXd surplus_distances = calculate_distance(sorted_led_pos(i), led_avg_center);

        // Finds the local minimum distance
        Eigen::MatrixXf::Index minRow, minCol;
        double temp_min = surplus_distances.minCoeff(&minRow, &minCol);
        // Updates the minima and the minima variables
        if (temp_min < min_distance_led_deficit)
        {
          min_distance_led_deficit = temp_min;
          min_distance_col = minCol;
          min_distance_row = minRow;
          min_distance_cluster = i;
        }
      }
    }
    // Moves data from one cluster to another, if the distance is acceptable
    if (min_distance_led_deficit < max_led_to_cluster_distance)
    {
      // Creates an ordered vector with the cluster points
      int num_leds = leds_clusters.sum();

      List2DPoints led_all_organized;
      led_all_organized.resize(num_leds);


      // Used to keep track of index
      int index_start = 0;
      for (int i = 0; i < clusters; i++)
      {
        led_all_organized.segment(index_start,leds_clusters(i)) = sorted_led_pos(i);
        index_start = index_start + leds_clusters(i);
      }

      // Finds the distance from the point of interest (POI) to the rest of the points
      Eigen::MatrixXd poi_distances = calculate_distance_single_point(sorted_led_pos(min_distance_cluster)(min_distance_row), led_all_organized);
      //std::cout << poi_distances << std::endl;

      // Number of nearest neighbours
      int knn_num = 3;
      Eigen::MatrixXd knn_idx(knn_num, 1);
      Eigen::MatrixXd knn_distance(knn_num, 1);

      // Sets the distance from the POI to itself to infinity
      Eigen::MatrixXf::Index minRow, minCol;
      double temp_min = poi_distances.minCoeff(&minRow, &minCol);
      poi_distances(minRow,minCol) = INFINITY;

      // Finds the K least distances
      for (int k = 0; k < knn_num; k++)
      {
        // Finds the local minimum distance
        temp_min = poi_distances.minCoeff(&minRow, &minCol);

        // Saves the index and distance
        knn_idx(k,0) = minCol;
        knn_distance(k,0) = temp_min;

        // Sets the distance to infinity
        poi_distances(minRow,minCol) = INFINITY;
      }

      // Finds out which cluster has the most NN
      Eigen::MatrixXd knn_count = Eigen::MatrixXd::Zero(clusters, 1);
      Eigen::MatrixXd knn_min_distance = Eigen::MatrixXd::Zero(clusters, 1);
      for (int k = 0; k < knn_num; k++)
      {
        index_start = 0;
        for (int i = 0; i < clusters; i++)
        {
          int index_end = index_start + leds_clusters(i);
          if((knn_idx(k) >= index_start) && (knn_idx(k) < index_end))
          {
            knn_count(i,0) = knn_count(i,0) + 1;
            if ( (knn_distance(k,0) < knn_min_distance(i,0)) || (knn_min_distance(i,0) != 0) )
            {
              knn_min_distance(i,0) = knn_distance(k,0);
            }
          }
          index_start = index_end;
        }
      }

      // Find the cluster with the most NN
      double knn_max_count = knn_count.maxCoeff();
      temp_min = INFINITY;
      int closest_cluster = -1;
      for (int i = 0; i < clusters; i++)
      {
        if (knn_count(i) == knn_max_count)
        {
          if (knn_min_distance(i) < temp_min)
          {
            temp_min = knn_min_distance(i);
            closest_cluster = i;
          }
        }
      }

      // Moves the data point to the closest cluster
      // Transfers point to new cluster, and updates number of LED's in each cluster
      if (min_distance_cluster != closest_cluster)
      {
        transferPoint(sorted_led_pos, min_distance_cluster, min_distance_row, closest_cluster);
        transferPoint(sorted_distortion_centers, min_distance_cluster, min_distance_row, closest_cluster);
        leds_clusters(min_distance_cluster) = leds_clusters(min_distance_cluster) - 1;
        leds_clusters(closest_cluster) = leds_clusters(closest_cluster) + 1;
        //std::cout << "Moving point" << std::endl;

        // Updates the new center of the cluster
        calculateCenter(sorted_led_pos, cluster_centers);
      }
      else
      {
        flag_move_point = false;
      }
    }
    else
    {
      flag_move_point = false;
    }

  }
  else
  {
    flag_move_point = false;
  }

}

void LEDDetector::transferPoint(VectorList2DPoints& matrix, unsigned int clusterToRemove, unsigned int pointIdxToTransfer, unsigned int clusterToReceive)
{
  unsigned int numClusters = matrix.size();
  if ((clusterToRemove < numClusters) && (clusterToReceive < numClusters))
  {
    unsigned int numRowsRemove = matrix(clusterToRemove).size();
    unsigned int numRowsReceive = matrix(clusterToReceive).size();

    // Doesnt seem like the index check works
    if (pointIdxToTransfer < numRowsRemove)
    {
      // Saves point in a temp
      Eigen::Vector2d temp_point;
      temp_point = matrix(clusterToRemove)(pointIdxToTransfer);

      // Moves the data from the cluster
      matrix(clusterToRemove).segment(pointIdxToTransfer, numRowsRemove - pointIdxToTransfer - 1) = matrix(clusterToRemove).segment(pointIdxToTransfer + 1, numRowsRemove - pointIdxToTransfer - 1);

      // Resizes the cluster and saves the data at the new position
      matrix(clusterToRemove).conservativeResize(numRowsRemove - 1);
      matrix(clusterToReceive).conservativeResize(numRowsReceive + 1);
      matrix(clusterToReceive)(numRowsReceive) = temp_point;
    }
  }
}


void LEDDetector::calculateCenter(VectorList2DPoints& matrix, List2DPoints& cluster_centers_out)
{
  /*Calculates the centers for the input vectorlist*/

  // Finds number of clusters
  unsigned int num_clusters = matrix.size();

  // Resizes cluster center
  cluster_centers_out.resize(num_clusters);

  // Runs through all the clusters
  for (int i = 0; i < num_clusters; i++)
  {
    double sum_x = 0, sum_y = 0;
    // Point to hold the new center of the cluster
    Eigen::Vector2d temp_point;
    for (int j = 0; j < matrix(i).size(); j++)
    {
      sum_x += matrix(i)(j)(0);
      sum_y += matrix(i)(j)(1);
    }
    temp_point(0) = sum_x / matrix(i).size();
    temp_point(1) = sum_y / matrix(i).size();
    cluster_centers_out(i) = temp_point;
  }
}

void LEDDetector::printVectorList(VectorList2DPoints& matrix)
{
  // Trying to display sorted data
  for (int i = 0; i < matrix.size(); i++)
  {
    std::cout << "\n Cluster ";
    std::cout << i;
    std::cout << "\n";
    for (int j = 0; j < matrix(i).size(); j++)
    {
      for (int k = 0; k < matrix(i)(j).size(); k++)
      {
        std::cout << matrix(i)(j)(k);
        std::cout << " ";
      }
      std::cout << "\n";
    }
  }
}

void LEDDetector::printList(List2DPoints& matrix)
{
  std::cout << "\n List: ";
  for (int j = 0; j < matrix.size(); j++)
  {
    for (int k = 0; k < matrix(j).size(); k++)
    {
      std::cout << matrix(j)(k);
      std::cout << " ";
    }
    std::cout << "\n";
  }
}


void LEDDetector::findCenterCorrespondence(RowXs& cluster_correspondences, List2DPoints& cluster_centers_old, List2DPoints& cluster_centers_new, double& max_relative_cluster_distance)
{
  /* Finds the correspondence between the old and the new center */

  // Number of new clusters
  int clusters = cluster_centers_new.rows();

  // Finds distance between the old cluster centers and new cluster centers
  Eigen::MatrixXd center_distances = calculate_distance(cluster_centers_old, cluster_centers_new);

  // Finds correspondences between old and new centers, based on the distance
  // cluster_correspondences.resize(cluster_centers_old.rows());
  // cluster_correspondences = Eigen::ArrayXXi::Zero(1, cluster_centers_old.rows()) - 1;
  //cluster_correspondences.resize(cluster_centers_new.rows());
  //Ucluster_correspondences = Eigen::ArrayXXi::Zero(1, cluster_centers_new.rows()) - 1;
  Eigen::MatrixXd temp_distances; // Might be able to skip the temp and use the real distance matrix
  temp_distances = center_distances;
  int temp_runtimes = (clusters > cluster_centers_old.rows() ? cluster_centers_old.rows() : clusters);
  for (int i = 0; i < temp_runtimes; i++)
  {
    if (temp_distances.rows() != 0)
    {
      // Finds minimum in distance matrix
      Eigen::MatrixXd::Index minRow, minCol;
      double min = temp_distances.minCoeff(&minRow, &minCol);
      // Ensures cluster is within some distance
      if (min < max_relative_cluster_distance)
      {
        cluster_correspondences(minRow) = minCol;
        temp_distances.row(minRow) = Eigen::ArrayXXd::Zero(1, temp_distances.cols()) + max_relative_cluster_distance + 1;
        temp_distances.col(minCol) = Eigen::ArrayXXd::Zero(temp_distances.rows(), 1) + max_relative_cluster_distance + 1;
      }
    }
  }
}


void LEDDetector::distributePointToDeficit(bool& flag_move_point, const int& num_led_objects, Eigen::MatrixXd& leds_clusters, VectorList2DPoints& sorted_led_pos, List2DPoints& cluster_centers, VectorList2DPoints& sorted_distortion_centers, double& max_led_to_cluster_distance)
{
  /* Move a data point from a cluster with many points, to a cluster with few point, unless the distance is to high */
  // Finds number of clusters
  int clusters = sorted_led_pos.size();

  // Creates flag to indicate if any cluster has more than five leds
  int cluster_overload = 0;
  // Creates flag to indicate if any cluster has less than five leds
  int cluster_underload = 0;
  // Checks the led amount in each cluster
  for (int i = 0; i < clusters; i++)
  {
    // Checks if a cluster has more than 5 leds
    if (leds_clusters(i) > num_led_objects)
    {
      cluster_overload = cluster_overload + 1;
    }
    // Checks if a cluster has less than 5 leds
    if (leds_clusters(i) < num_led_objects)
    {
      cluster_underload = cluster_underload + 1;
    }
  }


  // If a cluster has a surplus and another has a deficit of LEDS, the cluster with surplus should give out leds
  if (cluster_overload > 0 && cluster_underload > 0)
  {
    // Creates variable to keep track of when the global minima distance has been reached
    double min_distance_led_deficit = max_led_to_cluster_distance + 1;
    int min_distance_row;
    int min_distance_col;
    int min_distance_cluster;

    // Creates variable to hold the centers of the cluster with deficit leds
    List2DPoints led_deficit_center;
    led_deficit_center.resize(cluster_underload);
    int temp_idx = 0;
    // Runs through all clusters and finds cluster with deficit leds
    for (int i = 0; i < clusters; i++)
    {
      // Finds cluster with deficit LEDS
      if (leds_clusters(i) < num_led_objects)
      {
        // Puts the deficit centers into a matrix
        led_deficit_center(temp_idx) = cluster_centers(i);
        temp_idx += 1;
      }

    }
    for (int i = 0; i < clusters; i++)
    {
      // Finds cluster with surplus LEDS
      if (leds_clusters(i) > num_led_objects)
      {
        // Calculates distance between surplus leds and deficit cluster centers
        Eigen::MatrixXd surplus_distances = calculate_distance(sorted_led_pos(i), led_deficit_center);

        // Finds the local minimum distance
        Eigen::MatrixXf::Index minRow, minCol;
        double temp_min = surplus_distances.minCoeff(&minRow, &minCol);
        // Updates the minima and the minima variables
        if (temp_min < min_distance_led_deficit)
        {
          min_distance_led_deficit = temp_min;
          min_distance_col = minCol;
          min_distance_row = minRow;
          min_distance_cluster = i;
        }
      }
    }
    // Moves data from one cluster to another, if the distance is acceptable
    if (min_distance_led_deficit < max_led_to_cluster_distance)
    {
      int temp_decifit_cluster_idx = 0;
      for (int i = 0; i < clusters; i++)
      {
        // Finds cluster with deficit LEDS
        if (leds_clusters(i) < num_led_objects)
        {
          if (temp_decifit_cluster_idx == min_distance_col)
          {
            // Transfers point to new cluster, and updates number of LED's in each cluster
            transferPoint(sorted_led_pos, min_distance_cluster, min_distance_row, i);
            transferPoint(sorted_distortion_centers, min_distance_cluster, min_distance_row, i);

            leds_clusters(min_distance_cluster) = leds_clusters(min_distance_cluster) - 1;
            leds_clusters(i) = leds_clusters(i) + 1;


            // Updates the new center of the cluster
            calculateCenter(sorted_led_pos, cluster_centers);
          }
          temp_decifit_cluster_idx += 1;
        }
      }
    }
    else
    {
      flag_move_point = false;
    }
  }
  else
  {
    flag_move_point = false;
  }
}


} // namespace
