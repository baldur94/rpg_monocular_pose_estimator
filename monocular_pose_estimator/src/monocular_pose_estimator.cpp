// This file is part of RPG-MPE - the RPG Monocular Pose Estimator
//
// RPG-MPE is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RPG-MPE is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RPG-MPE.  If not, see <http://www.gnu.org/licenses/>.

/*
 * monocular_pose_estimator.cpp
 *
 * Created on: Jul 29, 2013
 * Author: Karl Schwabe
 */

/** \file monocular_pose_estimator_node.cpp
 * \brief File containing the main function of the package
 *
 * This file is responsible for the flow of the program.
 *
 */

#include "monocular_pose_estimator/monocular_pose_estimator.h"

namespace monocular_pose_estimator
{

/**
 * Constructor of the Monocular Pose Estimation Node class
 *
 */
MPENode::MPENode(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private)
  : nh_(nh), nh_private_(nh_private), have_camera_info_(false)
{
  // Set up a dynamic reconfigure server.
  // This should be done before reading parameter server values.
  dynamic_reconfigure::Server<monocular_pose_estimator::MonocularPoseEstimatorConfig>::CallbackType cb_;
  cb_ = boost::bind(&MPENode::dynamicParametersCallback, this, _1, _2);
  dr_server_.setCallback(cb_);

  // Initialize subscribers
  //image_sub_ = nh_.subscribe("/camera/image_raw", 1, &MPENode::imageCallback, this);
  //camera_info_sub_ = nh_.subscribe("/camera/camera_info", 1, &MPENode::cameraInfoCallback, this);
  
  image_sub_ = nh_.subscribe("/iri_mvbluefox3_camera/cam1/image_raw/", 1, &MPENode::imageCallback, this);
  camera_info_sub_ = nh_.subscribe("/iri_mvbluefox3_camera/cam1/camera_info/", 1, &MPENode::cameraInfoCallback, this);

  // Initialize pose publisher
  pose_pub_ = nh_.advertise<geometry_msgs::PoseWithCovarianceStamped>("estimated_pose", 1);

  // Initialize pose publisher
  poseVec_pub_ = nh_.advertise<monocular_pose_estimator::VectorOfPoseWithCovariance>("estimated_pose_vector", 1);

  // Initialize image publisher for visualization
  image_transport::ImageTransport image_transport(nh_);
  image_pub_ = image_transport.advertise("image_with_detections", 1);

  // Create the marker positions from the test points
  VectorList4DPoints positions_of_markers_on_object;

  // Read in the marker positions from the YAML parameter file
  XmlRpc::XmlRpcValue points_list;

  // Init number of active objects to zero
  num_active_objects_ = 0;


  iter_ = 0;

  if (!nh_private_.getParam("marker_positions", points_list))
  {
    ROS_ERROR(
        "%s: No reference file containing the marker positions, or the file is improperly formatted. Use the 'marker_positions_file' parameter in the launch file.",
        ros::this_node::getName().c_str());
    ros::shutdown();
  }
  else
  {
    // Get number of targets and resize
    positions_of_markers_on_object.resize(points_list.size());

    double back_projection_pixel_tolerance, nearest_neighbour_pixel_tolerance, certainty_threshold, valid_correspondence_threshold, z_axis_angle_threshold;

    if (!(nh_private_.getParam("back_projection_pixel_tolerance", back_projection_pixel_tolerance) && 
          nh_private_.getParam("nearest_neighbour_pixel_tolerance", nearest_neighbour_pixel_tolerance) && 
          nh_private_.getParam("certainty_threshold", certainty_threshold) &&
          nh_private_.getParam("valid_correspondence_threshold", valid_correspondence_threshold) &&
          nh_private_.getParam("z_axis_angle_threshold", z_axis_angle_threshold)))
  {
    ROS_ERROR(
        "%s: The parameters were not found. Check the launch file or the .cfg file.",
        ros::this_node::getName().c_str());
    ros::shutdown();
  }

    static const double obj_params_temp[] = {back_projection_pixel_tolerance, nearest_neighbour_pixel_tolerance, certainty_threshold, valid_correspondence_threshold, z_axis_angle_threshold};
    std::vector<double> obj_params (obj_params_temp, obj_params_temp + sizeof(obj_params_temp) / sizeof(obj_params_temp[0]) );


    // Initialize tracking objects
    trackable_object_.resize(points_list.size(), obj_params);
    // back_projection_pixel_tolerance, nearest_neighbour_pixel_tolerance, certainty_threshold, valid_correspondence_threshold, z_axis_angle_threshold
    // Run through all targets
    for (int i = 0; i < points_list.size(); i++)
    {
      // Set ID
      trackable_object_[i].setObjectID(points_list[i]["target"][0]["ID"]);

      // Get number of points for target and resize
      positions_of_markers_on_object[i].resize(points_list[i]["target"].size()-1); // -1 because ID is the first
      
      // Get LEDs per object
      if (i == 0)
        {
          // Set number of LEDs per object
          num_led_objects_ = positions_of_markers_on_object[i].size();
        }


      // Run trough all points
      for (int j = 0; j < positions_of_markers_on_object[i].size(); j++)
      {
        // Create temp
        Eigen::Matrix<double, 4, 1> temp_point;
        temp_point(0) = points_list[i]["target"][j+1]["x"]; // +1 because ID is the first
        temp_point(1) = points_list[i]["target"][j+1]["y"];
        temp_point(2) = points_list[i]["target"][j+1]["z"];
        temp_point(3) = 1;
        positions_of_markers_on_object[i][j] = temp_point;
       }
    }
  }

  // Set marker positions for all objects
  for (int i = 0; i < trackable_object_.size(); i++)
  {
    trackable_object_[i].setMarkerPositions(positions_of_markers_on_object[i]);

  }
  
  // Prompt information about number of targets and LEDS
  ROS_INFO("The number of targets is: %d", (int )positions_of_markers_on_object.size());

  for (int i = 0; i < positions_of_markers_on_object.size(); i++)
  {
    ROS_INFO("Target %s has %d number of markers", trackable_object_[i].getObjectID().c_str(), (int) positions_of_markers_on_object[i].size());
  }
}

/**
 * Destructor of the Monocular Pose Estimation Node class
 *
 */
MPENode::~MPENode()
{

}

/**
 * The callback function that retrieves the camera calibration information
 *
 * \param msg the ROS message containing the camera calibration information
 *
 */
void MPENode::cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr& msg)
{
  if (!have_camera_info_)
  {
    cam_info_ = *msg;

    // Calibrated camera
    camera_matrix_K_ = cv::Mat(3, 3, CV_64F);
    camera_matrix_K_.at<double>(0, 0) = cam_info_.K[0];
    camera_matrix_K_.at<double>(0, 1) = cam_info_.K[1];
    camera_matrix_K_.at<double>(0, 2) = cam_info_.K[2];
    camera_matrix_K_.at<double>(1, 0) = cam_info_.K[3];
    camera_matrix_K_.at<double>(1, 1) = cam_info_.K[4];
    camera_matrix_K_.at<double>(1, 2) = cam_info_.K[5];
    camera_matrix_K_.at<double>(2, 0) = cam_info_.K[6];
    camera_matrix_K_.at<double>(2, 1) = cam_info_.K[7];
    camera_matrix_K_.at<double>(2, 2) = cam_info_.K[8];
    camera_distortion_coeffs_ = cam_info_.D;

    // Set camera parameters for all objects
    for (int i = 0; i < trackable_object_.size(); i++)
    {
      trackable_object_[i].setCameraParm(camera_matrix_K_, camera_distortion_coeffs_);
    }

    // Resize number of published poses
    poseVec_msg.pose.resize(trackable_object_.size()); 
    poseVec_msg.ID.resize(trackable_object_.size());
    poseVec_msg.pose_found.resize(trackable_object_.size());

    have_camera_info_ = true;
    std::cout << "\n camera_matrix_K_: " << cam_info_.K[0] << std::endl;
    std::cout << "\n camera_distortion_coeffs_: " << cam_info_.D[0] << std::endl;


    ROS_INFO("Camera calibration information obtained.");

  }

}

/**
 * The callback function that is executed every time an image is received. It runs the main logic of the program.
 *
 * \param image_msg the ROS message containing the image to be processed
 */
void MPENode::imageCallback(const sensor_msgs::Image::ConstPtr& image_msg)
{
  // if (iter_ != -1) // Debugging
  // {   
  // iter_ += 1;
  // }///////

  // Start time
  ros::Time time_begin = ros::Time::now();

  // Check whether already received the camera calibration data
  if (!have_camera_info_)
  {
    ROS_WARN("No camera info yet...");
    return;
  }

  // Import the image from ROS message to OpenCV mat
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::MONO8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  cv::Mat image = cv_ptr->image;

  // Get time at which the image was taken. This time is used to stamp the estimated pose and also calculate the position of where to search for the makers in the image
  double time_to_predict = image_msg->header.stamp.toSec();

  // Set up pixel positions list
  List2DPoints detected_led_positions;

  // Create List2DPoints variable for distorted points (only for visualization)
  List2DPoints distorted_detection_centers;

  // Full ROI
  region_of_interest_ = cv::Rect(0, 0, image.cols, image.rows);


  // Search image within ROI
  LEDDetector::findLeds(image, region_of_interest_, detection_threshold_value_, gaussian_sigma_, min_blob_area_,
                        max_blob_area_, max_width_height_distortion_, max_circular_distortion_,
                        detected_led_positions, distorted_detection_centers, camera_matrix_K_,
                        camera_distortion_coeffs_);

  //std::cout << "\n detected_led_positions size: " << detected_led_positions.size() << std::endl;

  // Create vector with index of active targets
  RowXu idx_active_objects(num_active_objects_);


  // Get index of active targets
  int j = 0;
  for (int i = 0; i < trackable_object_.size(); i++)
  {
    if (trackable_object_[i].getIsActive())
    {
      idx_active_objects(j) = i;
      j += 1;
    }
  }


  // Check if any LED is detected
  if(detected_led_positions.size() != 0)
  {
    // LEDs were found

    // Create List2DPoints variable to get cluster centers
    List2DPoints cluster_centers_old(num_active_objects_);

    // Get cluster centers
    for (int i = 0; i < num_active_objects_; i++)
    {
      cluster_centers_old[i] = trackable_object_[idx_active_objects(i)].getClusterCenter();
    }
    // Make copy
    List2DPoints cluster_centers_new = cluster_centers_old;

    // Create row variable that tells which input "centers" and output "sorted_led_pos" that matches
    // Initialize to -1
    RowXs cluster_correspondences = Eigen::ArrayXXi::Zero(1, num_active_objects_) - 1;

    // Create VectorList2DPoints variable for sorted_led_pos
    VectorList2DPoints sorted_led_pos;
    VectorList2DPoints sorted_distorted_led_pos; 

    // Cluster LEDs
    LEDDetector::kmeans_cluster(num_led_objects_, detected_led_positions, cluster_centers_new, sorted_led_pos, distorted_detection_centers, sorted_distorted_led_pos, cluster_correspondences, max_relative_cluster_distance_, max_led_to_cluster_distance_, min_point_density_threshold_);

    // Create variable to keep track of used clusters
    RowXu idx_used_clusters(cluster_centers_new.size());

    // Estimate pose for all active objects
    int num_of_used_clusters = 0;

    // Temp to contain num of active clusters that are now inactive
    j = 0;
    for (int i = 0; i < num_active_objects_; i++)
    {
      // Check if new cluster exists
      if (cluster_correspondences(i) != -1)
      {
        // Try to estimate pose and check if succes 
        if (trackable_object_[idx_active_objects(i)].estimateBodyPose(sorted_led_pos[cluster_correspondences(i)], time_to_predict, image.size(), roi_border_thickness_))
        {
          //Succes

          // Update cluster center 
          trackable_object_[idx_active_objects(i)].setClusterCenter(cluster_centers_new[cluster_correspondences(i)]);
          
          // Update object points 
          trackable_object_[idx_active_objects(i)].setDistortedDetectionCenters(sorted_distorted_led_pos[cluster_correspondences(i)]);
          

          // Update used cluster idx
          idx_used_clusters(num_of_used_clusters) = cluster_correspondences(i); // i
          num_of_used_clusters += 1;
        }
        else
        {
          //Fail

          // Set inactive
          trackable_object_[idx_active_objects(i)].setIsActive(false); 

          // One less active object
          j += 1;
        }
      }
      else
      {
        // No cluster exists

        // Set inactive
        trackable_object_[idx_active_objects(i)].setIsActive(false);
        
        // One less active object
        j += 1;
      }
    }

    // Update number of active objects
    num_active_objects_ -= j;

    // Resize idx_used_clusters
    idx_used_clusters.conservativeResize(num_of_used_clusters);

    // Create vector with index of non active targets
    int num_of_non_active_objects = trackable_object_.size()-num_active_objects_;

    RowXu idx_non_active_objects(num_of_non_active_objects);

    // Get index of non active targets
    j = 0;
    for (int i = 0; i < trackable_object_.size(); i++)
    {
      if (trackable_object_[i].getIsActive() == false)
      {
        idx_non_active_objects(j) = i;
        j += 1;
      }
    }
    // Create vector with index of unused cluster
    int num_of_unused_clusters = cluster_centers_new.size()-num_of_used_clusters; 
    RowXu idx_unused_clusters(num_of_unused_clusters);


    // Create temp variale vector containing  [0:max]
    RowXu temp1(num_of_unused_clusters);
    temp1 = RowXu::LinSpaced(cluster_centers_new.size(),0,cluster_centers_new.size()-1);
    // Make copy of idx_used_clusters 
    RowXu temp2 = idx_used_clusters;
    // Sort the temp
    std::sort(temp2.data(), temp2.data() + temp2.size(), [](int lhs, int rhs){return rhs > lhs;});
    // Find the difference
    auto it = std::set_difference(temp1.data(), temp1.data() + temp1.size(),
                                  temp2.data(), temp2.data() + temp2.size(),  
                                  idx_unused_clusters.data());
    // Resize
    idx_unused_clusters.conservativeResize(std::distance(idx_unused_clusters.data(), it)); // resize the result



    // Check if there are any unused clusters and objects
    if ((num_of_unused_clusters != 0) && (num_of_non_active_objects != 0))
    {
      MatrixXYu permutation_matrix = (num_of_unused_clusters < num_of_non_active_objects ? 
        Combinations::permutationsNoReplacement(num_of_non_active_objects,num_of_non_active_objects) : Combinations::permutationsNoReplacement(num_of_unused_clusters, num_of_non_active_objects));

      // Create error matrix for all permutations of targets and clusters
      MatrixXYd error_matrix(num_of_unused_clusters, num_of_non_active_objects);
      error_matrix = MatrixXYd::Zero(num_of_unused_clusters, num_of_non_active_objects);

      // Create 1/0 found pose matrix for all permutations of targets and clusters
      MatrixXYu found_pose_matrix(num_of_unused_clusters, num_of_non_active_objects);
      found_pose_matrix = MatrixXYu::Zero(num_of_unused_clusters, num_of_non_active_objects);

      std::cout << "9" << std::endl;
      
      // Calculate error for all combinations
      double error = INFINITY;

      // All unused clusters
      for (int i = 0; i < num_of_unused_clusters; i++)
      {
        // All non active objects
        for (int j = 0; j < num_of_non_active_objects; j++)
        {
          // LEDDetector::printList(sorted_led_pos[idx_unused_clusters(i)]);
          if(trackable_object_[idx_non_active_objects(j)].initEstimateBodyPose(sorted_led_pos[idx_unused_clusters(i)], time_to_predict, error))
          {
            error_matrix(i,j) = error;
            found_pose_matrix(i,j) = 1;
          }
          else
          {
            error_matrix(i,j) = error;
            found_pose_matrix(i,j) = 0; 
          }
          error = INFINITY; // Not needed
        }
      }
      //std::cout << "\n error_matrix: " << error_matrix;

      
      // List for number of found poses in a permutation
      RowXu num_of_poses_in_perm_list(permutation_matrix.rows());
      num_of_poses_in_perm_list = RowXu::Zero(permutation_matrix.rows());

      // List for the error in a permutation
      RowXd error_of_poses_in_perm_list(permutation_matrix.rows());
      error_of_poses_in_perm_list = RowXd::Zero(permutation_matrix.rows());

      // Run through all permutations
      for (int i = 0; i < permutation_matrix.rows(); i++)
      {
        // Count number of poses and sum error
        for (int j = 0; j < permutation_matrix.cols(); j++)
        {
          if (permutation_matrix(i,j) > num_of_unused_clusters)
          {
            // Cluster doesn't exist
          }
          else
          {
            // Cluster exists
            num_of_poses_in_perm_list(i) += found_pose_matrix(permutation_matrix(i,j)-1,j);
            error_of_poses_in_perm_list(i) += error_matrix(permutation_matrix(i,j)-1,j);
          } 
        }
      }

      // Find min error at max found poses
      int idx_min_error = -1;
      double max_poses = 0;
      double min_error = INFINITY;
      // Run through all permutations 


      //std::cout << "\nnum_of_poses_in_perm_list.size(): " << num_of_poses_in_perm_list.size();

      for (int i = 0; i < num_of_poses_in_perm_list.size(); i++)
      {
        // Check if more poses are found
        //std::cout << "\n(i): " << i;
        //std::cout << "\nnum_of_poses_in_perm_list(i): " << num_of_poses_in_perm_list(i);
        //std::cout << "\nerror_of_poses_in_perm_list(i): " << error_of_poses_in_perm_list(i);

        if (num_of_poses_in_perm_list(i) > max_poses)
        {
          max_poses = num_of_poses_in_perm_list(i);
          min_error = error_of_poses_in_perm_list(i);
          idx_min_error = i; 
        } // Check if num of poses is the same and error has decreased
        else if ((num_of_poses_in_perm_list(i) == max_poses) && (error_of_poses_in_perm_list(i) < min_error))
        {
          min_error = error_of_poses_in_perm_list(i);
          idx_min_error = i; 
        }
      }
      //std::cout << "\nidx_min_error: " << idx_min_error;
      //std::cout << "\n min_error: " << min_error << std::endl;

      // Run objects with best clusters
      if (idx_min_error == -1)
      {
        // No poses were found
      }
      else
      {
        for (int i = 0; i < permutation_matrix.cols(); i++)
        {
          // Check if pose was found previously
          if ((permutation_matrix(idx_min_error,i) <= num_of_unused_clusters) && (found_pose_matrix(permutation_matrix(idx_min_error,i)-1,i) == 1))
          {
            
            // Get pose and print if failed
            if(trackable_object_[idx_non_active_objects(i)].estimateBodyPose(sorted_led_pos[idx_unused_clusters(permutation_matrix(idx_min_error,i)-1)], time_to_predict, image.size(), roi_border_thickness_))
            {

              // Update cluster center 
              trackable_object_[idx_non_active_objects(i)].setClusterCenter(cluster_centers_new[idx_unused_clusters(permutation_matrix(idx_min_error,i)-1)]);
            
              // Count up active objects
              num_active_objects_ += 1;

            } // Shuold not be possible to enter here
            else 
            {
              std::cout << "\n FAIL FAIL FAIL";
            }
            
          }
        }
      }
     
    }


    // Get index of active targets
    idx_active_objects.resize(num_active_objects_);

    j = 0;
    for (int i = 0; i < trackable_object_.size(); i++)
    {
      if (trackable_object_[i].getIsActive())
      {
        idx_active_objects(j) = i;
        j += 1;
      }
    }

    
  }
  else
  {
    // Set all object to non-active
    for (int i = 0; i < idx_active_objects.size(); i++)
    {
      trackable_object_[idx_active_objects(i)].setIsActive(false);
    }

    num_active_objects_ = 0;
  }


  // // Zero pose and pose_found
  for (int i = 0; i < trackable_object_.size(); i++)
  {
    // Zero pose
    poseVec_msg.pose[i].pose.position.x = 0;
    poseVec_msg.pose[i].pose.position.y = 0;
    poseVec_msg.pose[i].pose.position.z = 0;
    poseVec_msg.pose[i].pose.orientation.x = 0;
    poseVec_msg.pose[i].pose.orientation.y = 0;
    poseVec_msg.pose[i].pose.orientation.z = 0;
    poseVec_msg.pose[i].pose.orientation.w = 0;

    // Zero pose_found
    poseVec_msg.pose_found[i] = false;

    // Zero covariance
    for (unsigned g = 0; g < 6; ++g)
    {
      for (unsigned j = 0; j < 6; ++j)
      {
        poseVec_msg.pose[i].covariance.elems[j + 6 * g] = 0;
      }
    }
  }

  
  // Check if minimum one object is active
  if (num_active_objects_ > 0)
  {
    // Update ID
    for (int i = 0; i < trackable_object_.size(); i++)
    {
      poseVec_msg.ID[i] = trackable_object_[i].getObjectID();
    }

    // Run through all active objects
    for (int i = 0; i < num_active_objects_; i++)
    {
      //Eigen::Matrix4d transform = trackable_object.getPredictedPose();
      Matrix6d cov = trackable_object_[idx_active_objects(i)].getPoseCovariance();
      Eigen::Matrix4d transform = trackable_object_[idx_active_objects(i)].getPredictedPose();

      ROS_DEBUG_STREAM("The transform: \n" << transform);
      ROS_DEBUG_STREAM("The covariance: \n" << cov);

      poseVec_msg.pose[idx_active_objects(i)].pose.position.x = transform(0, 3);
      poseVec_msg.pose[idx_active_objects(i)].pose.position.y = transform(1, 3);
      poseVec_msg.pose[idx_active_objects(i)].pose.position.z = transform(2, 3);
      Eigen::Quaterniond orientation = Eigen::Quaterniond(transform.block<3, 3>(0, 0));
      poseVec_msg.pose[idx_active_objects(i)].pose.orientation.x = orientation.x();
      poseVec_msg.pose[idx_active_objects(i)].pose.orientation.y = orientation.y();
      poseVec_msg.pose[idx_active_objects(i)].pose.orientation.z = orientation.z();
      poseVec_msg.pose[idx_active_objects(i)].pose.orientation.w = orientation.w();

      // Show euler angle
      //Eigen::Vector3d ea = transform.block<3, 3>(0, 0).eulerAngles(2, 1, 0);
      //std::cout << "\n ea: \n" << ea;
      
      // Show angle for z axis
      // Eigen::Vector3d norm_z_vector(0,0,1);
      // Eigen::Vector3d rot_z_vector = transform.block<3, 3>(0, 0)*norm_z_vector;
      // double z_axis_angle = acos(norm_z_vector.transpose()*rot_z_vector);
      // std::cout << "\n z axis angle: " << z_axis_angle ;

      // Add covariance to PoseWithCovarianceStamped message
      for (unsigned g = 0; g < 6; ++g)
      {
        for (unsigned j = 0; j < 6; ++j)
        {
          poseVec_msg.pose[idx_active_objects(i)].covariance.elems[j + 6 * g] = cov(g, j);
        }
      }

      // Update pose_found
      poseVec_msg.pose_found[idx_active_objects(i)] = true;
    }
  }
  else
  { // If pose was not updated
    ROS_WARN("Unable to resolve any pose.");
  }

  
  poseVec_msg.header.stamp = image_msg->header.stamp;
  poseVec_msg.num_of_targets = trackable_object_.size();


  // End time
  ros::Time time_end = ros::Time::now();

  // Update compute tim
  poseVec_msg.compute_time = time_end.toSec()-time_begin.toSec();

  // Publish
  poseVec_pub_.publish(poseVec_msg);

  
  // publish visualization image
  if (image_pub_.getNumSubscribers() > 0)
  {
    cv::Mat visualized_image = image.clone();
    cv::cvtColor(visualized_image, visualized_image, CV_GRAY2RGB);
    

    // Check if minimum one object is active
    if (num_active_objects_ > 0)
    {
      // Run through all targets
      for (int i = 0; i < num_active_objects_; i++)
      {
        trackable_object_[idx_active_objects(i)].augmentImage(visualized_image, idx_active_objects(i));  
      }
    }

    // Publish image for visualization
    cv_bridge::CvImage visualized_image_msg;
    visualized_image_msg.header = image_msg->header;
    visualized_image_msg.encoding = sensor_msgs::image_encodings::BGR8;
    visualized_image_msg.image = visualized_image;

    image_pub_.publish(visualized_image_msg.toImageMsg());
  }
  std::cout << "\n---------------------------------------------------\n";
  
}


/**
 * The dynamic reconfigure callback function. This function updates the variable within the program whenever they are changed using dynamic reconfigure.
 */
void MPENode::dynamicParametersCallback(monocular_pose_estimator::MonocularPoseEstimatorConfig &config, uint32_t level)
{
  detection_threshold_value_ = config.threshold_value;
  gaussian_sigma_ = config.gaussian_sigma;
  min_blob_area_ = config.min_blob_area;
  max_blob_area_ = config.max_blob_area;
  max_width_height_distortion_ = config.max_width_height_distortion;
  max_circular_distortion_ = config.max_circular_distortion;
  roi_border_thickness_ = config.roi_border_thickness;
  max_relative_cluster_distance_ = config.max_relative_cluster_distance;
  max_led_to_cluster_distance_ = config.max_led_to_cluster_distance;
  min_point_density_threshold_ = config.min_point_density_threshold;

  for (int i = 0; i < trackable_object_.size(); i++)
  {
    trackable_object_[i].setBackProjectionPixelTolerance(config.back_projection_pixel_tolerance);
    trackable_object_[i].setNearestNeighbourPixelTolerance(config.nearest_neighbour_pixel_tolerance);
    trackable_object_[i].setCertaintyThreshold(config.certainty_threshold);
    trackable_object_[i].setValidCorrespondenceThreshold(config.valid_correspondence_threshold);
    trackable_object_[i].setZAxisAngleThreshold(config.z_axis_angle_threshold);
  }

  ROS_INFO("Parameters changed");
}













} // namespace monocular_pose_estimator

